<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PlaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create three entities
        
        DB::table('places')->insert([
            'name' =>'First place',
            'address' => 'Address first place',
            'city' => 'City FIrst place',
            'state' => 'State First place',
            'latitude' =>  28.452763,
            'longitude' => -81.412228
        ]);
        
        DB::table('places')->insert([
            'name' => 'Second place',
            'address' => 'Address second place',
            'city' => 'City second place',
            'state' => 'State second place',
            'latitude' =>  27.452763,
            'longitude' => -80.412228
        ]);
        
        DB::table('places')->insert([
            'name' => 'third place',
            'address' => 'Address third place',
            'city' => 'City third place',
            'state' => 'State third place',
            'latitude' =>  28.412763,
            'longitude' => -81.422228
        ]);
    }
}
