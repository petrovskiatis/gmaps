
require('./bootstrap');

window.Vue = require('vue').default;



// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

import * as VueGoogleMaps from 'vue2-google-maps';

Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyAPpeiD6GH96mJx8idkhTv5UWlVQShFXK8'
    }
});

const app = new Vue({
    el: '#app',
    data() {
        return {
            places: [],
            infoWindowOptions: {
                pixelOffset: {
                    width: 0,
                    height: -35
                }
            },
            activePlace: {},
            infoWindowOpened: false
        }
    },
    created () {
        axios.get('/api/places')
                .then((AxiosResponse) => this.places = AxiosResponse.data)
                .catch((error) => console.error(error));
    },
    methods: {
        getPosition(place){
            return {
                lat: parseFloat(place.latitude),
                lng: parseFloat(place.longitude)
            }
        },
        handleMarkerClicked(p) {
            this.activePlace = p;
            this.infoWindowOpened = true;
        },
        handleInfoWindowClose() {
            this.activePlace = {};
            this.infoWindowOpened = false;
        },
        handleMapClick(e) {
            this.places.push({
                name: '',
                address: '',
                city: '',
                state: '',
                latitude: e.latLng.lat(),
                longitude: e.latLng.lng()
            });
            
//            axios.post('/api/places/create')
//                    .then({
//               latitude: e.latLng.lat(),
//               longitude: e.latLng.lng()
//            });
        }
    },
    computed: {
        mapCenter() {
            if (!this.places.length) {
                return {
                    lat: 10,
                    lng: 10
                }
            }
            
            return {
                lat: parseFloat(this.places[0].latitude),
                lng: parseFloat(this.places[0].longitude)
            }
        },
        infoWindowPosition() {
            return {
                lat: parseFloat(this.activePlace.latitude),
                lng: parseFloat(this.activePlace.longitude)
            }
        }
    }
});
